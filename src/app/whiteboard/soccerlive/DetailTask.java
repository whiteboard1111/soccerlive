package app.whiteboard.soccerlive;
import java.io.InputStream;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailTask extends AsyncTask<Void, Void, Void>{
	
	private Context mActivity;
	private String mLink;
	private String mImgURL;
	private Drawable img;
	private String descr;
	private ImageView mImage;
	private TextView mDescription;
	private ProgressDialog mProgressDialog;
	
	//コンストラクタ
	public DetailTask(Context context, 
					  String link, 
					  String imgURL, 
					  ImageView image, 
					  TextView description) {
		
		mActivity = context;
		mLink = link;
		mImgURL = imgURL;
		mImage = image;
		mDescription = description;
	}
	
	@Override
	protected void onPreExecute(){
		//プログレスバーを表示する
		mProgressDialog = new ProgressDialog(mActivity);
		mProgressDialog .setMessage("Now Loading...");
		mProgressDialog.show();
	}
	
	//本文取得
	@Override
	protected Void doInBackground(Void... params) {
		//本文を取得する
		descr = loadDesc(mLink);
		img = loadImage(mImgURL);
		
		return null;
	}
	
	protected void onPostExecute(Void huge){
		//テキストビューの設置
		mImage.setBackgroundDrawable(img);
		mDescription.setText(descr);
		//Dialogの接続
		mProgressDialog.dismiss();
	}

	//本文テキスト取得メソッド
		private String loadDesc(String link){
			Document doc = null;
			Elements detailDesc = null;
			CharSequence detailDescription = null;
			try{
				
				doc = Jsoup.connect(link.toString()).get();
				detailDesc = doc.select("#article");
				detailDescription = detailDesc.text();
			
			}catch(Exception e){
			}
			return (String) detailDescription;
		}
		
		//画像の取得
		private Drawable loadImage(String imgURL){
			Drawable image = null;
			try{
				URL url = new URL(imgURL.toString());
				InputStream in;
				in = url.openStream();
				image = Drawable.createFromStream(in, "test");
				in.close();
			}catch(Exception e){
		}
		return image;
	}
	
}
