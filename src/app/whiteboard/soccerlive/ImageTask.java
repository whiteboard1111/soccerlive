package app.whiteboard.soccerlive;

import java.io.InputStream;
import java.net.URL;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

public class ImageTask extends AsyncTask<Void, Void, Void> {

	private RssListAdapter mAdapter;
	private RssReaderActivity mActivity;
	private ProgressDialog mProgressDialog;

	public ImageTask(RssReaderActivity activity, RssListAdapter adapter) {
		// TODO 自動生成されたコンストラクター・スタブ
		mActivity = activity;
		mAdapter = adapter;
	}

	@Override
	protected void onPreExecute() {
		// プログレスバーを表示する
		mProgressDialog = new ProgressDialog(mActivity);
		mProgressDialog.setMessage("Now Loading...");
		mProgressDialog.show();
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO 自動生成されたメソッド・スタブ
		this.ImageParse();
		return null;
	}

	protected void onPostExecute(Void huge) {
		mProgressDialog.dismiss();
	}

	private void ImageParse() {

		Item currentItem;
		// とりあえず20回回しとく
		for (int i = 0; i <= 19; i++) {
			currentItem = mAdapter.getItem(i);
			// 画像の取得
			try {
				URL url = new URL(currentItem.getImgURL().toString());
				InputStream in;
				in = url.openStream();
				Drawable image = Drawable.createFromStream(in, "test");
				in.close();
				currentItem.setImage(image);
			} catch (Exception e) {
			}

		}
	}

}
