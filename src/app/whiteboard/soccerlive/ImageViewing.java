package app.whiteboard.soccerlive;

import java.io.InputStream;
import java.net.URL;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

public class ImageViewing extends AsyncTask<String, Drawable, Drawable>{

	private ImageView mImage;
	private Item mItem;
	private String mImageURL;
	private String tag;
	private int getColumn;

	public ImageViewing(ImageView imageView,Item item, int position) {
		mImage = imageView;
		mItem = item;
		getColumn = position;
		this.tag = imageView.getTag().toString();
	}

	@Override
	protected Drawable doInBackground(String... params) {
		// TODO 自動生成されたメソッド・スタブ
		mImageURL = params[0];
		//画像の取得
		try{
			URL url = new URL(mImageURL.toString());
			InputStream in;
			in = url.openStream();
			Drawable image = Drawable.createFromStream(in, "test");
			in.close();
			return image;
		}catch(Exception e){
		}
		
	
		return null;
	
}
	
	protected void onPostExecute(Drawable a){
		
		if(this.tag.equals(this.mImage.getTag())){
			if(mImage != null){
				mImage.setBackgroundDrawable(a);
			}
	 }
	}

	

}
