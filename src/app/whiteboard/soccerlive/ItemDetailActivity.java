package app.whiteboard.soccerlive;

import java.lang.reflect.Field;

import tk.app.rssreader.R;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemDetailActivity extends Activity {
	private TextView mTitle;
	private TextView mDate;
	private TextView mDescr;
	private ImageView mImage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.item_detail);

		// 強制でオーバーフロウメニューを追加する
		try {
			// Viewの設定値取得
			ViewConfiguration cnf = ViewConfiguration.get(this);
			// 端末にメニューボタンがあるか確認
			Field field = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (field != null) {
				// フィールドアクセスの許可
				field.setAccessible(true);
				// 端末にメニューボタンが無いとして設定
				field.setBoolean(cnf, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Intent intent = getIntent();

		// 詳細画面タイトルセット
		String title = intent.getStringExtra("TITLE");
		mTitle = (TextView) findViewById(R.id.item_detail_title);
		mTitle.setText(title);

		// 詳細画面日時セット
		String pubdate = intent.getStringExtra("DATETIME");
		mDate = (TextView) findViewById(R.id.item_detail_date);
		if (mDate == null) {
			Log.i("test3", "test3");
		}
		mDate.setText(pubdate);
		// 詳細画面画像、本文をセット
		final String link = intent.getStringExtra("LINK");
		String imgURL = intent.getStringExtra("imgURL");
		mImage = (ImageView) findViewById(R.id.item_detail_image);
		// DetailTask detailTask = new DetailTask(this, link, imgURL, mImage,
		// mDescr);
		// detailTask.execute();

		// 記事の本文をセット
		String description = intent.getStringExtra("DESCRIPTION");
		mDescr = (TextView) findViewById(R.id.item_detail_descr);
		mDescr.setText(description);

		// ボタンクリック処理
		Button btnLink = (Button) findViewById(R.id.item_detail_link_button);
		btnLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}
}