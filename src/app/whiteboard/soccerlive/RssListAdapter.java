package app.whiteboard.soccerlive;

import java.util.List;

import tk.app.rssreader.R;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RssListAdapter extends ArrayAdapter<Item> {
	private LayoutInflater mInflater;
	private TextView mTitle;
	private TextView mDate;
	private ImageView mImage;
	private TextView mSiteName;

	// コンストラクタ
	public RssListAdapter(Context context, List<Item> objects) {
		super(context, 0, objects);
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	// 1行ごとのビューを生成する
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// convertviewがnullであればレイアウト読み込み、あれば使いまわす
		View view = convertView;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.item_row, null);
		}

		// 現在参照しているリストの位置からItemを取得する
		Item item = this.getItem(position);
		if (item != null) {

			// Itemからタイトルデータを取り出し、TextViewにセットする
			String title = item.getTitle().toString();
			mTitle = (TextView) view.findViewById(R.id.item_title);
			mTitle.setText(title);

			// 日時を取り出し、ビューにセット
			String date = item.getDateTime().toString();
			mDate = (TextView) view.findViewById(R.id.item_date);
			mDate.setText(date);

			// Itemから本文データを取り出し、TextViewにセットする
			// String headLine = item.getHeadLine().toString();
			// mHeadLine = (TextView) view.findViewById(R.id.item_descr);
			// mHeadLine.setText(headLine);

			// Itemから提供元データを取り出しセットする
			String siteName = item.getProvided().toString();
			mSiteName = (TextView) view.findViewById(R.id.item_site_name);
			mSiteName.setText(siteName);

			// Itemから画像データを取り出し、TextViewにセットする
			Drawable image = item.getImage();
			mImage = (ImageView) view.findViewById(R.id.item_image);
			mImage.setTag(item.getImgURL());
			mImage.setBackgroundDrawable(image);

			// 画像を張り付けるだけのスレッド、setDrawableするだけ。使ったほうが早いかも？
			// ImageViewing imgTask = new ImageViewing(mImage, item, position);
			// imgTask.execute(item.getImgURL().toString());

		}
		return view;
	}
}