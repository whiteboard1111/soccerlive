package app.whiteboard.soccerlive;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

public class RssParserTask extends AsyncTask<SiteData, Integer, RssListAdapter> {
	private RssReaderActivity mActivity;
	private RssListAdapter mAdapter;
	private ProgressDialog mProgressDialog;

	/*
	 * コンストラクタ 引数のactivityはプログレスバーの表示に必要なもの adapterはresultで結果を返すために必要
	 */
	public RssParserTask(RssReaderActivity activity, RssListAdapter adapter) {
		mActivity = activity;
		mAdapter = adapter;
	}

	// プログレスバーを表示するためにコールする
	@Override
	protected void onPreExecute() {
		// プログレスバーを表示する
		mProgressDialog = new ProgressDialog(mActivity);
		mProgressDialog.setMessage("Now Loading...");
		mProgressDialog.show();
	}

	// サイトのデータを取得し、parseXmlに渡してパース結果を取得する
	@Override
	protected RssListAdapter doInBackground(SiteData... params) {

		RssListAdapter result = null;
		SiteData mSiteData = new SiteData();
		ArrayList<Site> SiteData = new ArrayList<Site>(mSiteData.siteData);

		try {
			result = parseXml(SiteData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// メインスレッド上で実行される
	@Override
	protected void onPostExecute(RssListAdapter result) {
		mProgressDialog.dismiss();
		mActivity.setListAdapter(result);
	}

	// XMLをパースする
	public RssListAdapter parseXml(ArrayList<Site> mProvidedSites)
			throws IOException, XmlPullParserException {

		// 記事を回した回数
		int getSiteTimes = 0;
		int getArticleTimes;
		while (getSiteTimes <= 1) {
			XmlPullParser parser = Xml.newPullParser();
			Site currentSite = mProvidedSites.get(getSiteTimes);
			URL url = new URL(currentSite.getSiteURL());
			InputStream is = url.openConnection().getInputStream();
			try {
				parser.setInput(is, null);
				int eventType = parser.getEventType();
				Item currentItem = null;
				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tag = null;
					switch (eventType) {
					case XmlPullParser.START_TAG:
						tag = parser.getName();
						if (tag.equals("item")) {
							// アイテム要素内
							currentItem = new Item();
							// 暫定でぶち込んでおく。後にサイトデータから渡す
							currentItem.setProvided(currentSite.getSiteName());
						} else if (currentItem != null) {
							if (tag.equals("title")) {
								currentItem.setTitle(parser.nextText());
							} else if (tag.equals("description")) {
								currentItem.setDescription(parser.nextText());
							} else if ((tag.equals("pubDate"))
									|| (tag.equals("pubdate"))) {
								currentItem.setDateTime(parser.nextText());
								Log.i("test5", currentItem.getDateTime()
										.toString());
							} else if (tag.equals("link")) {
								currentItem.setLink(parser.nextText());
							} else if (tag.equals("enclosure")) {
								currentItem.setImgURL(parser.nextText());
								Log.i("test", currentItem.getImgURL()
										.toString());
							}
						}
						break;
					case XmlPullParser.END_TAG:
						tag = parser.getName();
						if (tag.equals("item")) {

							mAdapter.add(currentItem);
						}
						break;

					}
					eventType = parser.next();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			getSiteTimes++;

		}
		return mAdapter;
	}
}