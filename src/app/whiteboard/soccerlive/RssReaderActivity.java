package app.whiteboard.soccerlive;

import java.lang.reflect.Field;
import java.util.ArrayList;

import tk.app.rssreader.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ListView;

public class RssReaderActivity extends ListActivity {
	private static final String RSS_FEED_URL = "http://www.soccer-king.jp/RSS.rdf";
	private RssListAdapter mAdapter;
	private ArrayList<Item> mItems;
	private SiteData mSiteData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);


		// 強制でオーバーフロウメニューを追加する
		try {
			// Viewの設定値取得
			ViewConfiguration cnf = ViewConfiguration.get(this);
			// 端末にメニューボタンがあるか確認
			Field field = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (field != null) {
				// フィールドアクセスの許可
				field.setAccessible(true);
				// 端末にメニューボタンが無いとして設定
				field.setBoolean(cnf, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Itemオブジェクトを保持するためのリストを生成し、アダプタに追加する
		mItems = new ArrayList<Item>();
		mAdapter = new RssListAdapter(this, mItems);
		// mProvidedSites = new SiteData();

		// パース処理タスクを起動
		RssParserTask task = new RssParserTask(this, mAdapter);
		task.execute(mSiteData);

		// 画像処理タスクを起動
		ImageTask imgTask = new ImageTask(this, mAdapter);
		imgTask.execute();

	}

	//アクションバーにメニューを追加
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}

	// 詳細画面移動用のインテント達
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Item item = mItems.get(position);
		Intent intent = new Intent(this, ItemDetailActivity.class);
		intent.putExtra("TITLE", item.getTitle());
		intent.putExtra("DESCRIPTION", item.getDescription());
		intent.putExtra("PUBDARE", item.getDateTime());
		intent.putExtra("LINK", item.getLink());
		intent.putExtra("getposition", position);
		intent.putExtra("imgURL", item.getImgURL().toString());
		startActivity(intent);
	}
}
