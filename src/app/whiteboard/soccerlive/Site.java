package app.whiteboard.soccerlive;


public class Site {
	public String SiteName;
	public String SiteURL;

	public Site() {
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getSiteURL() {
		return SiteURL;
	}

	public void setSiteURL(String siteURL) {
		SiteURL = siteURL;
	}
}
