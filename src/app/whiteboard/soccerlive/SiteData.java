package app.whiteboard.soccerlive;

import java.util.ArrayList;

public class SiteData {

	ArrayList<Site> siteData = new ArrayList<Site>();

	Site soccerKing = new Site();
	Site nikkanSports = new Site();

	public SiteData() {

		// サッカーキングのデータ
		soccerKing.setSiteName("サッカーキング");
		soccerKing.setSiteURL("http://www.soccer-king.jp/RSS.rdf");
		siteData.add(soccerKing);

		// 日刊スポーツのデータ
		nikkanSports.setSiteName("日刊スポーツ");
		nikkanSports
				.setSiteURL("http://feed.rssad.jp/rss/nikkansports/soccer/world/index.rdf");
		siteData.add(nikkanSports);

	}

}
